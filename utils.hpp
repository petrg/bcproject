// contains custom string representation used throughout this project (pointer to char with no null-termination coupled with length)
// implements printing of normal output and warnings

#ifndef UTILS
#define UTILS
#include <string>
#include <cstring>
#include <iostream>

namespace parsing {

struct str {
    char* ptr;
    int length;
    static char terminator;

    str(char* c_str, int length):ptr(c_str),length(length) {}

    char& operator[](int index) {
        return ptr[index];
    }

    const char& operator[](int index) const {
        return ptr[index];
    }

    explicit str(std::string& s):ptr(&s[0]),length(s.length()) {}
    explicit str(char* c_str):ptr(c_str),length(strlen(c_str)) {}
    str():ptr(&terminator),length(0) {}
    bool operator<(const str& other) const;
    bool compare(const char* other, int s_len) const;
    bool operator==(const str& other) const;
    bool operator==(const char* c_str) const;
    bool operator==(const std::string& s) const;
};

std::ostream& operator<<(std::ostream& os, const str s);
std::string toString(str);

void flushOutput();
void print(const char* s);
void print(str s);
void print(const std::string& s);
void println();
void println(const char* s);
void println(str s);
void println(const std::string& s);

class Warning {
  public:
    template<typename... R> static void print(const char* first, R*... rest) {
        flushOutput();
        printInternal(first,rest...);
        fprintf(stderr,"\n");
    }
    template<typename...R> static void printWithInput(str* inputLine, R*... args) {
        flushOutput();
        fprintf(stderr,"warning: \"");
        printInternal(args...);
        printInternal("\" occurred when processing line \"",inputLine,"\"\n");
    }

  private:
    static void printInternal() {
        fprintf(stderr,"%s","\n");
    }
    template<typename... R> static void printInternal(const char* first, R*... rest) {
        fprintf(stderr,"%s",first);
        printInternal(rest...);
    }
    template<typename... R> static void printInternal(const str* first, R*... rest) {
        char* end = first->ptr + first->length;
        char back = *end;
        *end = '\0';
        fprintf(stderr,"%s",first->ptr);
        *end = back;
        printInternal(rest...);
    }
};

class DebugPrinter {
  public:
    DebugPrinter();
    void enable();
    void disable();
    void incIndent();
    void decIndent();
    bool isEnabled() const;
    template<typename... T> void print(T*... strs) const {
        if (!_enabled) return;
        parsing::print(_indent.c_str());
        printInternal(strs...);
        println();
    }

  private:
    std::string _indent;
    bool _enabled;

    void printInternal() const {}
    template<typename... R> void printInternal(const char* first, R*... rest) const {
        parsing::print(first);
        printInternal(rest...);
    }

    template<typename... R> void printInternal(const str* first, R*... rest) const {
        parsing::print(*first);
        printInternal(rest...);
    }
};

using timeType = unsigned long long;

}
#endif
