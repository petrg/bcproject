// implementation of the *context* object - stores attributes by name

#ifndef CONTEXT
#define CONTEXT
#include <map>
#include <iostream>
#include <vector>
#include <list>
#include <memory>
#include "utils.hpp"

namespace parsing {
struct Result {
    str* attributes;
    std::unique_ptr<std::list<std::string>> attributeOwner;
    std::unique_ptr<std::map<str,str>> additionalAttributes;

    Result(str* attrs, std::unique_ptr<std::list<std::string>> owner, std::unique_ptr<std::map<str,str>> additional);
    Result(Result&&) noexcept;
    Result(const Result&) noexcept = delete;
    ~Result();
};

class Context {
  public:
    explicit Context(str originalMsg);
    ~Context();
    void setAttribute(str name, str attribute);
    str& getAttribute(str name);
    str& getAttribute(const std::string& name);
    void addOrSetAttr(const char* name, std::string value);
    void setAttribute(int index, bool isFinal, str value);
    void setTimestamp(timeType timestamp);
    timeType getTimestamp() const;
    str& getAttribute(int index, bool isFinal);
    Result extractResults();
    void reinitialize(str originalMsg);
    const str* getFinalAttributes() const;
    const std::map<str,str>* getAdditionalAttributes() const;
    str& getOriginalMsg();
    void setPriority(char* facility, char* severity);
    static const std::map<str,int>& getFinalAttributeIndexes();
    static bool isFinal(str attrName);
    static bool isFinal(const std::string& attrName);
    static int addAttribute(std::string name);

  private:
    static std::map<str,int> _finalAttributeIndexes;
    static std::map<str,int> _intermediateAttributeIndexes;
    static std::list<std::string> _attributeNames;
    str* _finalAttributes;
    str* _intermediateAttributes;
    timeType _timestamp;
    std::string _timeString;
    str _timeStr;
    std::unique_ptr<std::list<std::string>> _attributeOwner;
    std::unique_ptr<std::map<str,str>> _additionalAttributes;
    str _originalMsg;
    char _facility[3];
    char _severity[3];
};
}
#endif
