#include "GrokIntegration.hpp"
#include "ConfigParser.hpp"
#include "Context.hpp"
#include "utils.hpp"
#include "python/python_integration.hpp"
#include <stdlib.h>
#include <fcntl.h>
#include <thread>
#include <chrono>
#include <map>
#include <functional>
#include <dirent.h>
#include <fstream>
#include <unordered_map>
#include <unordered_set>
#include <sys/socket.h>
#include <sys/un.h>
#include <getopt.h>
#include <libgen.h>
#include <cerrno>

parsing::ParseAction* parsingTree;
parsing::Context* context;

class Reader {
  public:
    Reader(int fd) : _fd(fd),_offsetBuffer(_originalBuffer),_offsetBufferSize(_bufferSize),_charsToRead(0),_charsRead(0),
      _charsInBuffer(0),_lastNewline(-1) {}

    parsing::str readLine() {
        char* newlinePtr = nextNewlinePtr();
        if (newlinePtr == nullptr) {
            readToBuff();
            if (_charsRead == 0) return parsing::str();
            newlinePtr = nextNewlinePtr();
        }
        char* line = _originalBuffer+_lastNewline+1;
        int length  = newlinePtr-line;
        if (length > _bufferSize) {
            throw std::runtime_error("internal: read line longer than the buffer it was read from");
        }
        _lastNewline += length+1;
        return parsing::str(line,length);
    }

    int charsRead() {
        return _charsRead;
    }

    bool bufferFull() {
        return _charsRead == _charsToRead;
    }

  private:
    int _fd;
    static constexpr int _bufferSize = 4096;
    char _originalBuffer[_bufferSize];

    //when all lines in the buffer are processed, there is an incomplete line at the end, this part gets copied to the beginning
    //_offsetBuffer is offset by this copied prefix for next read()
    char* _offsetBuffer;
    int _offsetBufferSize;
    int _charsToRead, _charsRead, _charsInBuffer;
    int _lastNewline;

    char* nextNewlinePtr() {
        return (char*)memchr(_originalBuffer+_lastNewline+1,'\n',_charsInBuffer-_lastNewline-1);
    }

    void moveLeftover() {
            int leftoverSize = _charsInBuffer-_lastNewline-1;//number of the chars to be copied to the begining
            int leftoverBegin = _lastNewline+1;
            if (_lastNewline >= _bufferSize-1) {//the last char of the buffer is newline, no copying necessary
                _offsetBuffer = _originalBuffer;
                _offsetBufferSize = _bufferSize;
                _charsInBuffer = 0;
                return;
            }
            memmove(_originalBuffer,_originalBuffer+leftoverBegin,leftoverSize);
            _charsInBuffer = leftoverSize;
            _lastNewline = -1;
            _offsetBuffer = _originalBuffer+leftoverSize;//updating for the next iteration
            _offsetBufferSize = _bufferSize-leftoverSize;
    }

    void readToBuff() {
        moveLeftover();
        bool lineSkipped = false;//to avoid duplicating warnings when skipping a long line
        do {
            _charsToRead = _offsetBufferSize-1;
            _charsRead = read(_fd,_offsetBuffer,_charsToRead);

            //for receiving messages that are shorter than the buffer and come one by one (through a socket)
            if (_charsRead > 0 && _charsRead < _charsToRead && _offsetBuffer[_charsRead-1] != '\n') _offsetBuffer[_charsRead++] = '\n';

            _charsInBuffer += _charsRead;
            if (_charsRead > 0 && memchr(_offsetBuffer,'\n',_charsInBuffer) == nullptr) {
                if (!lineSkipped) std::cerr << "line longer than the buffer, skipping" << std::endl;
                lineSkipped = true;
            } else {
                lineSkipped = false;
            }
        } while (lineSkipped);
    }
};

class DirMonitor {
  public:
    DirMonitor(std::string path, std::string pattern,
                std::function<bool(const std::string&,const std::string&)> filenameComparator, bool cleanRun,
                std::string processedFilesStorage)
      : _dirname(std::move(path)),_filter(std::move(pattern),std::vector<std::string>()),_currentFd(-1)
      ,_comparator(std::move(filenameComparator)) {
        if (_dirname.rfind("/") == _dirname.size()-1) _dirname.erase(_dirname.size()-1);
        std::ifstream processedFiles(processedFilesStorage);
        if (processedFiles.good() && !cleanRun) {
            ino_t inode = 0;
            off_t offset = 0;
            while (true) {
                processedFiles >> inode >> offset;
                if (processedFiles.fail()) {
                    if (!processedFiles.eof()) {
                        std::cerr << "warning: progress storage open but unreadable, some files may be processed again" << std::endl;
                    }
                    break;
                }
                _offsets.emplace(inode,offset);
            }
        } else {
            if (!processedFiles.good() && !processedFilesStorage.empty()) {
                std::cerr << "warning: could not open progress storage for reading, some files may be processed again" << std::endl;
            }
        }
        processedFiles.close();
        _processedFilesStorage.open(processedFilesStorage,std::ios_base::trunc);
        _registeredInstances.insert(this);
    }

    ~DirMonitor() {
        _registeredInstances.erase(this);
        flushProcessedFiles();
    }

    void flushProcessedFiles() noexcept {
        closeCurrentFile();
        for (const auto& offset : _offsets) {
            if (_foundFilesInodes.find(offset.first) != _foundFilesInodes.end())
                _processedFilesStorage << offset.first << "\t" << offset.second << std::endl;
        }
    }

    const auto& update() {
        DIR* _dir = opendir(_dirname.c_str());
        if (_dir == nullptr) throw std::runtime_error("could not open dir "+_dirname+": "+std::strerror(errno));
        _foundFiles.clear();
        struct dirent* entry = nullptr;
        while ((entry = readdir(_dir)) != nullptr) {
            std::string filename(entry->d_name);
            if (_filter.matches(parsing::str(filename))) {
                struct stat stats;
                std::string filePath = _dirname+"/"+filename;
                if (stat(filePath.c_str(),&stats) == -1) {
                    throw std::runtime_error("stat failed for file "+filePath+": "+std::strerror(errno));
                }
                if (!S_ISREG(stats.st_mode)) continue;
                _foundFiles.push_back(std::move(filePath));
                _foundFilesInodes.insert(stats.st_ino);
            }
        }
        closedir(_dir);
        std::sort(_foundFiles.begin(),_foundFiles.end(),_comparator);
        return _foundFiles;
    }

    int openFile(const std::string& path) {
        closeCurrentFile();
        _currentFd = open(path.c_str(),0);
        if (_currentFd < 0) throw std::runtime_error("could not open file "+path+": "+std::strerror(errno));
        struct stat stats;
        if (fstat(_currentFd,&stats) == -1) throw std::runtime_error("stat failed after opening "+path+": "+std::strerror(errno));
        ino_t inode = stats.st_ino;
        _currentIndode = inode;
        auto it = _offsets.find(inode);
        if (it == _offsets.end()) {
            _offsets.emplace(inode,0);
        } else {
            lseek(_currentFd,it->second,SEEK_SET);
        }
        return _currentFd;
    }

    static void flushAllInstances() {
        for (auto* instance : _registeredInstances) instance->flushProcessedFiles();
    }

  private:
    std::string _dirname;
    parsing::GrokIntegration _filter;
    std::function<bool(const std::string&, const std::string&)> _comparator;
    std::unordered_map<ino_t,off_t> _offsets;
    std::unordered_set<ino_t> _foundFilesInodes;
    std::vector<std::string> _foundFiles;
    std::ofstream _processedFilesStorage;
    int _currentFd;
    ino_t _currentIndode;
    static std::unordered_set<DirMonitor*> _registeredInstances;

    void closeCurrentFile() {
        if (_currentFd > 0) {
            _offsets[_currentIndode] = lseek(_currentFd,0,SEEK_CUR);
            if (close(_currentFd) == -1) {
                throw std::runtime_error("could not close file descriptor "+std::to_string(_currentFd)+": "+std::strerror(errno));
            }
        }
    }
};
std::unordered_set<DirMonitor*> DirMonitor::_registeredInstances;

long getNum(const std::string& s) {
    size_t begin = s.size();
    for (int i=s.size(); i>=0; i--) {
        if (std::isdigit(s[i])) begin = i;
        if (!std::isdigit(s[i])) break;
    }
    char* end;
    return std::strtol(s.c_str()+begin,&end,10);
}

std::function<bool(const std::string&,const std::string&)> lexicalSort = [](const auto& a, const auto& b) {return a < b;};

std::function<bool(const std::string&,const std::string&)> numericSort =
    [](const auto& a, const auto& b) {return getNum(a) < getNum(b);};

parsing::str extractPriority(parsing::str& s) {
    char* numEnd = nullptr;
    if (s.ptr[0] != '<') return parsing::str();
    long priVal = std::strtol(s.ptr+1,&numEnd,10);
    int numLength = numEnd-s.ptr;
    if (numLength >= s.length || *numEnd != '>') return parsing::str();
    parsing::str ret(s.ptr+1,numLength-1);
    s.ptr += numLength+1;
    s.length -= numLength+1;
    return ret;
}

void readFrom(int fd, bool monitor, bool priorityIncluded = false) {
    Reader reader(fd);
    bool first = true;
    do {
        if (!first) std::this_thread::sleep_for(std::chrono::seconds(1));
        first = false;
        parsing::str s;
        while (true) {
            s = reader.readLine();
            if (reader.charsRead() == 0) break;
            if (priorityIncluded) {
                auto priValStr = extractPriority(s);
                context->reinitialize(s);
                char* end = priValStr.ptr+priValStr.length;
                char backup = *end;
                *end = 0;
                char* priValEnd = nullptr;
                int priVal = std::strtol(priValStr.ptr,&priValEnd,10);
                *end = backup;
                if (priValEnd != end) parsing::Warning::print("could not parse priority value ",&priValStr);
                int severity = priVal%8;
                int facility = priVal/8;
                char severityStr[16];
                char facilityStr[16];
                snprintf(severityStr,sizeof(severityStr),"%d",severity);
                snprintf(facilityStr,sizeof(facilityStr),"%d",facility);
                context->setPriority(facilityStr,severityStr);
            } else {
                context->reinitialize(s);
            }
            parsingTree->perform(*context);
            parsing::python::timeoutSessions(context->getTimestamp());
            if (!reader.bufferFull()) parsing::flushOutput();
        }
        if (!monitor) parsing::python::endAllSessions();
        parsing::flushOutput();
    } while (monitor);
}

int socketFd = -1;
std::string sockPath;

void readFromStdin(bool monitor) {
    readFrom(0,monitor);
}

void readFromSocket(const parsing::ConfigParser::options& options, bool monitor) {
    int sfd = socket(AF_UNIX,SOCK_DGRAM,0);
    if (sfd == -1) throw std::runtime_error(std::string("call to socket() failed: ")+std::strerror(errno));
    struct sockaddr_un addr;
    memset(&addr,0,sizeof(struct sockaddr_un));
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path,options.readFrom.c_str(),sizeof(addr.sun_path)-1);
    if (bind(sfd,(struct sockaddr*)(&addr),sizeof(struct sockaddr_un)) == -1) {
        throw std::runtime_error("could not bind socket to path "+options.readFrom+": "+std::strerror(errno));
    }
    sockPath = options.readFrom;
    socketFd = sfd;
    do {
        readFrom(socketFd,false,true);
    } while (monitor);
}

void readFromDir(const parsing::ConfigParser::options& options, bool monitor, bool cleanRun) {
    const std::string& path = options.readFrom;
    auto slashPos = path.rfind("/");
    auto pattern = "^"+path.substr(slashPos+1,path.length()-slashPos)+"$";
    std::string arg = path.substr(0,slashPos);
    auto sortOrder = options.sortOrder == "num" ? numericSort : lexicalSort;
    DirMonitor dirMonitor(arg,std::move(pattern),sortOrder, cleanRun, options.progressStorage);
    do {
        const auto& files = dirMonitor.update();
        for (int i=0; i<files.size(); i++) {
            std::cerr << "reading from file " << files[i] << std::endl;
            readFrom(dirMonitor.openFile(files[i]),false);
            std::cerr << "done reading from file " << files[i] << std::endl;
        }
        if (monitor) std::this_thread::sleep_for (std::chrono::seconds(1));
    } while (monitor);
}

void finalize() noexcept {
    try{
        if (socketFd > 0 && !sockPath.empty()) {
            unlink(sockPath.c_str());
            remove(sockPath.c_str());
        }
        Py_Finalize();
        parsing::flushOutput();
    }catch(std::exception& e) {
        std::cerr << e.what() << std::endl;
    }
}

void termHandler(int num) {
    DirMonitor::flushAllInstances();
    finalize();
    exit(num);
}

static std::pair<struct option, std::string> options[] = {
    {{"debug",no_argument,nullptr,0}, "Turns on debugging prints (both for configuration parsing and for reading input"},
    {{"monitor",no_argument,nullptr,0}, "When the whole input is read, do not terminate but wait for the input to grow.\nOverrides the monitor behavior from the config."},
    {{"no-monitor",no_argument,nullptr,0}, "Do no monitoring. Overrides the monitor behavior from the config."},
    {{"clean-run",no_argument,nullptr,0}, "Ignore progress storage when starting up and read all files from the beginning."},
    {{"help",no_argument,nullptr,0}, "Print this help and exit."},
    {{nullptr,no_argument,nullptr,0}, ""}
};

void printUsage(const char* name) {
    std::cout << "usage:" << std::endl << name;
    for (int i=0; options[i].first.name != nullptr; i++) {
        std::cout << " [--" << options[i].first.name << "]";
    }
    std::cout << " configpath" << std::endl;
    std::cout << "\n";
    for (int i=0; options[i].first.name != nullptr; i++) {//prints option descriptions in a nice format
        std::cout << options[i].first.name;
        std::string message = options[i].second;
        auto printedChars = strlen(options[i].first.name);
        while (!message.empty()) {
            auto newlinePos = message.find("\n");
            std::string partToPrint = message.substr(0,newlinePos);
            if (newlinePos == std::string::npos) {
                message.clear();
            } else {
                message.erase(0,newlinePos+1);
            }
            std::string padding;
            int paddingLength = 20-printedChars;
            if (paddingLength < 0) paddingLength = 0;
            padding.append(paddingLength,' ');
            std::cout << padding << partToPrint << std::endl;
            printedChars = 0;
        }
        std::cout << std::endl;
    }
}

int main(int argc, char** argv) {
    try{
        setenv("TZ","UTC+0",1);
        signal(SIGINT,termHandler);
        signal(SIGTERM,termHandler);

        //argument parsing
        struct {
            bool debug = false;
            int monitorOverride = 0;
            bool cleanRun = false;
        } args;
        int nOptions;
        for (nOptions=0; options[nOptions].first.name != nullptr; nOptions++);
        struct option rawOptions[nOptions];
        for (int i=0; i<nOptions; i++) rawOptions[i] = options[i].first;
        while (true) {
            int i = 0;
            int ret = 0;
            ret = getopt_long(argc,argv,"h",rawOptions,&i);
            if (ret == -1) break;
            if (ret == 'h') i = 4;
            switch (i) {
                case 0: args.debug = true; break;
                case 1: args.monitorOverride = 1; break;
                case 2: args.monitorOverride = -1; break;
                case 3: args.cleanRun = true; break;

                case 4:
                case 'h': 
                    printUsage(argv[0]);
                    return 0;
                    break;

                default:
                    std::cerr << "option \"" << rawOptions[i].name
                        << "\" cannot be handled - did you add it and forget to add handling?" << std::endl;
                    break;
            }
        }
        if (optind >= argc) {
            printUsage(argv[0]);
            return 1;
        }

        char configPathArg[strlen(argv[optind])+1];
        strcpy(configPathArg,argv[optind]);//dirname and basename may modify its argument, making a copy for them
        std::string configName(basename(configPathArg));
        std::string configBaseDir(dirname(argv[optind]));
        parsing::ConfigParser::setBaseDir(configBaseDir);

        std::string pythonPath;
        char* origPythonPath = getenv("PYTHONPATH");
        if (origPythonPath != nullptr) pythonPath.append(origPythonPath);
        if (!pythonPath.empty()) pythonPath.append(":");
        pythonPath.append(configBaseDir);//looking for user scripts in the dir with the config file
        pythonPath.append(":/usr/local/lib/beaver");//for importing session.py
        setenv("PYTHONPATH",pythonPath.c_str(),1);

        //config parsing
        std::string mainTreeName = configName+":main";
        parsing::ConfigParser parser(std::move(configName));
        char* libDir = getenv("BEAVER_LIBPATH");
        if (libDir != nullptr) {
            std::string newBaseDir(libDir);
            parsing::ConfigParser::setBaseDir(newBaseDir);
            pythonPath.append(":"+newBaseDir);
            setenv("PYTHONPATH",pythonPath.c_str(),1);
        }
        if (args.debug) {
            parsing::ConfigParser::enableDebug();
            parsing::ParseAction::enableDebug();
        }
        parser.parse();
        parsing::ConfigParser::check();
        parsingTree = parsing::TreeStorage::find(mainTreeName);
        parsing::str null;
        parsing::Context c(null);
        context = &c;
        bool monitor = args.monitorOverride == 1 || (parser.getOptions().implicitMonitor && args.monitorOverride == 0);
        if (args.debug) {
            parsing::println();
            parsing::print("done parsing config");
            parsing::println();
            parsing::flushOutput();
        }

        auto& parserOptions = parser.getOptions();
        if (parserOptions.readFrom.empty()) {
            readFromStdin(monitor);
        } else if (parserOptions.openSocket) {
            readFromSocket(parserOptions,monitor);
        } else {
            readFromDir(parserOptions,monitor,args.cleanRun);
        }

        finalize();
        return 0;

    } catch(std::exception& e) {
        finalize();
        std::cerr << argv[0] << ": " << e.what() << std::endl;
        return 1;
    }
}
