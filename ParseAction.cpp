// contains the implementation of most *actions* (*call* and *execute* are in CallAction.hpp)

#include <map>
#include <time.h>
#include "MatchOption.hpp"
#include "ParseAction.hpp"
#include "utils.hpp"

namespace parsing {

//class ParseAction

std::vector<std::unique_ptr<ParseAction>> ParseAction::_actionOwner;
DebugPrinter ParseAction::_debug;

ParseAction* ParseAction::storeAction(std::unique_ptr<ParseAction> action) {
    _actionOwner.push_back(std::move(action));
    return _actionOwner.back().get();
}
void ParseAction::enableDebug() {
    _debug.enable();
}

//class Root

Root::Root(std::string treeName) : _treeName(treeName) {}

Root::Root(std::string treeName, std::vector<ParseAction*> actions) : _treeName(treeName),_actions(actions) {}

std::pair<Root*,std::vector<ParseAction*>&> Root::create(std::string treeName) {
    Root* instance = (Root*)ParseAction::storeAction(std::make_unique<Root>(std::move(treeName)));
    return {instance,instance->_actions};
}

void Root::perform(Context& context) {
    if (_debug.isEnabled()) {
        _debug.incIndent();
        _debug.print("entering tree ",_treeName.c_str());
    }
    for (ParseAction* action : _actions) {
        action->perform(context);
        if (Match::extractBreakFlag()) {
            _debug.print("match break flag was set, breaking");
            break;
        }
    }
    if (_debug.isEnabled()) {
        _debug.print("leaving tree ",_treeName.c_str());
        _debug.decIndent();
    }
}

void Root::addAction(ParseAction* action) {
    _actions.push_back(action);
}

//class Match

Match::Match(std::string subjectName,int subjectIndex, bool subjectIsFinal)
    : _subjectIndex(subjectIndex),_subjectIsFinal(subjectIsFinal),_subjectName(std::move(subjectName)),_instanceBreakFlag(false) {}

void Match::perform(Context& context) {
    if (_debug.isEnabled()) {
        _debug.incIndent();
        _debug.print("entering match of attribute \"",_subjectName.c_str(),"\"");
    }
    bool setBreakFlag = false;
    for (auto& option : _options) {
        _debug.print("matching attribute \"",_subjectName.c_str(),"\" with pattern \"",option->grok.getPattern().c_str(),"\"");
        _debug.print("matched attribute contains \"",&context.getAttribute(_subjectIndex,_subjectIsFinal),"\"");
        str subject = context.getAttribute(_subjectIndex,_subjectIsFinal);
        auto& actions = option->actions;
        if (option->grok.tryMatch(subject,context,*option)) {
            _debug.print("match succeeded");
            for (int i=0; i<actions.size(); i++) {
                ParseAction* action = actions[i];
                auto& actionAddr = actions[i];
                action->perform(context);
                if (Match::extractBreakFlag()) {
                    _debug.print("match break flag was set, breaking");
                    break;
                }
            }
            if (_instanceBreakFlag) {
                _debug.print("setting match break flag");
                setBreakFlag = true;
            }
            if (option->breakFlag) {
                _debug.print("option break flag is set, breaking");
                break;
            }
        }
    }
    if (setBreakFlag) _classBreakFlag = true;
    if (_debug.isEnabled()) {
        _debug.print("leaving match of attribute \"",_subjectName.c_str(),"\" ");
        _debug.decIndent();
    }
}

MatchOption& Match::addMatchOption(std::unique_ptr<MatchOption> option) {
    _options.push_back(std::move(option));
    return *_options.back();
}

void Match::setBreakFlag() {
    _instanceBreakFlag = true;
}

bool Match::extractBreakFlag() {
    bool ret = _classBreakFlag;
    _classBreakFlag = false;
    return ret;
}

bool Match::_classBreakFlag(false);

//class TimestampParser

TimestampParser::TimestampParser(std::string&& format, const std::string& attribute, const std::string& attribute2,
                                const std::string& offsetAttr)
  : _format(std::move(format)),_lastTimestamp(0),_lastTime() {
    str weak_attribute(const_cast<std::string&>(attribute));//const cast only to meet formal interface, no actual changes made
    str weak_attribute2(const_cast<std::string&>(attribute2));//again const for formal interface
    _attrIndexes.first = Context::addAttribute(attribute);
    _attrsFinal.first = Context::isFinal(weak_attribute);
    _attrIndexes.second = Context::addAttribute(attribute2);
    _attrsFinal.second = Context::isFinal(weak_attribute2);
    _offsetAttrIndex = Context::addAttribute(offsetAttr);
    str weakOffsetAttr(const_cast<std::string&>(offsetAttr));
    _offsetAttrFinal = Context::isFinal(weakOffsetAttr);
}

void TimestampParser::perform(Context& context) {
    struct tm time;
    memset(&time,0,sizeof(time));
    std::pair<str,str> attributes(context.getAttribute(_attrIndexes.first,_attrsFinal.first),
        context.getAttribute(_attrIndexes.second,_attrsFinal.second));
    _debug.print("parsing timestamp \"",&attributes.first,"\" by formatting string \"",_format.c_str(),"\"");

    char backup = attributes.first[attributes.first.length];
    attributes.first[attributes.first.length] = '\0';
    bool parseFailed = strptime(attributes.first.ptr,_format.c_str(),&time) == nullptr;
    attributes.first[attributes.first.length] = backup;

    str inputLine = context.getOriginalMsg();
    if (parseFailed) {
        Warning::printWithInput(&inputLine,"could not parse time \"",
            &attributes.first,"\" with format \"",_format.c_str(),"\"");
    }
    timeType timestamp = 0;
    if (time.tm_hour == _lastTime.tm_hour && time.tm_yday == _lastTime.tm_yday && time.tm_year == _lastTime.tm_year) {
        timestamp = _lastTimestamp + (time.tm_hour-_lastTime.tm_hour)*3600
            + (time.tm_min-_lastTime.tm_min)*60 + time.tm_sec-_lastTime.tm_sec;
    } else {
        timestamp = mktime(&time);
    }
    _lastTime = time;
    _lastTimestamp = timestamp;
    int i = 0;
    for (i=0; std::isspace(attributes.second[i]); i++);
    attributes.second.ptr += i;
    attributes.second.length -= i;
    if (attributes.second.length > 6) attributes.second.length = 6;

    char& end = attributes.second[attributes.second.length];
    backup = end;
    end = '\0';
    auto micros = std::atoi(attributes.second.ptr);
    if (attributes.second.length > 0 && !std::isdigit(attributes.second[attributes.second.length-1])) {
        Warning::printWithInput(&inputLine,"attribute for microseconds ends with non-digit character and will be ignored");
        micros = 0;
    }
    for (i=0; i<6-attributes.second.length; i++) micros *= 10;
    end = backup;

    str offset = context.getAttribute(_offsetAttrIndex,_offsetAttrFinal);
    backup = offset.ptr[offset.length];
    offset.ptr[offset.length] = '\0';
    char* hoursEnd = nullptr;
    long hours = strtol(offset.ptr,&hoursEnd,10);
    long minutes = 0;
    char* minutesEnd = nullptr;
    if (hoursEnd+1 < offset.ptr+offset.length) minutes = strtol(hoursEnd,&minutesEnd,10);
    if (hours < 0) minutes *= -1;
    offset.ptr[offset.length] = backup;

    timestamp -= 3600*hours+60*minutes;
    timestamp = 1000000*timestamp+micros;
    context.setTimestamp(timestamp);
}

//class Print

Print::Print(std::string firstLiteral) {
    addLiteral(std::move(firstLiteral));
}

void Print::addLiteral(std::string literal) {
    _literals.push_back(std::move(literal));
}

void Print::addAttributeToPrint(int index, bool isFinal) {
    _attributes.emplace_back(index,isFinal);
}

void Print::perform(Context& context) {
    auto max = _literals.size();
    if (_attributes.size() > max) max = _attributes.size();
    for (int i=0; i<max; i++) {
        if (i < _literals.size()) print(_literals[i]);
        if (i < _attributes.size()) print(context.getAttribute(_attributes[i].first,_attributes[i].second));
    }
    println();
}

//class ContextPrint

void ContextPrint::perform(Context& context) {
    const parsing::str* results = context.getFinalAttributes();
    const auto& resultIndexes = context.getFinalAttributeIndexes();
    for (const auto& indexPair : resultIndexes) {
        if (results[indexPair.second].length > 0) {
            print(indexPair.first);
            print(" : ");
            println(results[indexPair.second]);
        }
    }

    const auto* attrs = context.getAdditionalAttributes();
    if (attrs != nullptr) {
        for (const auto& attrPair : *attrs) {
            if (attrPair.second.length > 0) {
                print(attrPair.first);
                print(" : ");
                println(attrPair.second);
            }
        }
    }
    println();
}

}
