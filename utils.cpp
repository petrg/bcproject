#include "utils.hpp"
#include <unistd.h>
#include <cerrno>

namespace parsing {

bool str::compare(const char* other, int s_len) const {
    if (length != s_len) return false;
    for (int i=0; i<length; i++) if (ptr[i] != other[i]) return false;
    return true;
}
std::ostream& operator<<(std::ostream& os, const str s) {
    os.write(s.ptr,s.length);
    return os;
}
bool str::operator<(const str& other) const {
    for (int i=0; i<length && i<other.length; i++) {
        if (ptr[i] < other[i]) return true;
        if (ptr[i] > other[i]) return false;
    }
    return length < other.length;
}
bool str::operator==(const str& other) const {
    return compare(other.ptr,other.length);
}
bool str::operator==(const char* c_str) const {
    return compare(c_str,strlen(c_str));
}
bool str::operator==(const std::string& s) const {
    return compare(s.c_str(),s.length());
}
std::string toString(str s) {
    std::string ret;
    ret.reserve(s.length);
    for (int i=0; i<s.length; i++) ret.append(1,s[i]);
    return ret;
}
char str::terminator = 0;

constexpr int bufferSize = 4096;
static char buffer[bufferSize];
static int bufferEnd = 0;
void flushOutput() {
    auto written = write(1,buffer,bufferEnd);
    if (written < bufferEnd) throw std::runtime_error("could not write: "+std::string(std::strerror(errno)));
    bufferEnd = 0;
}
void print(const char* s, int length, bool escape = true) {
    for (int i=0; i<length; i++) {
        if (bufferEnd > bufferSize-4) flushOutput();
        if (escape && (s[i] < 32 || s[i] > 126)) {
            //need to escape
            buffer[bufferEnd++] = '\\';
            unsigned char c = s[i];
            buffer[bufferEnd++] = '0' + c/100;
            buffer[bufferEnd++] = '0' + (c%100)/10;
            buffer[bufferEnd++] = '0' + c%10;
        } else {
            buffer[bufferEnd++] = s[i];
        }
    }
}
void print(const char* s) {
    print(s,strlen(s));
}
void print(str s) {
    print(s.ptr,s.length);
}
void print(const std::string& s) {
    print(s.c_str());
}
void println() {
    print("\n",1,false);
}
void println(const char* s) {
    print(s);
}
void println(str s) {
    print(s);
    println();
}
void println(const std::string& s) {
    print(s);
    println();
}

//class DebugPrinter

DebugPrinter::DebugPrinter():_enabled(false) {}
void DebugPrinter::enable() {
    _enabled = true;
}
void DebugPrinter::disable() {
    _enabled = false;
}
void DebugPrinter::incIndent() {
    if (_enabled) _indent.append(4,' ');
}
void DebugPrinter::decIndent() {
    if (_enabled && _indent.size() > 0) _indent.erase(_indent.size()-4);
}
bool DebugPrinter::isEnabled() const {
    return _enabled;
}

}
