#include "MatchOption.hpp"

namespace parsing {

MatchOption::MatchOption(std::string&& pattern,const std::vector<std::string>& patternFiles)
    : grok(std::move(pattern),patternFiles),actions(),breakFlag(false) {}

}
