base=$(pwd)
export LD_LIBRARY_PATH=$base/python/build/lib.linux-x86_64-3.8:$(find /usr/lib -name libgrok.so | sed 's=/librok\.so$=='):$LD_LIBRARY_PATH
export PYTHONPATH=$base/python:$(ls -d $base/python/build/lib.*/ | tr '\n' ' ' | sed 's/[[:space:]]\+/:/g')$PYTHONPATH
