// *call* and *execute* action implementations - it is also responsible for importing modules and compiling inline code

#ifndef CALL_ACTION
#define CALL_ACTION

#include <Python.h>
#include <map>
#include "../Context.hpp"
#include "../ParseAction.hpp"

namespace parsing::python {

struct contextWrapper {
    PyObject_HEAD
    Context* context;
};

void init();
PyObject* getContextWrapper(Context* context);
void endAllSessions();

class CallAction : public ParseAction {
  public:
    CallAction(const std::string& functionId, const std::string& defaultModule);
    ~CallAction();
    bool isValid();
    virtual void perform(Context& context);
    static PyObject* getModule(const std::string& moduleName);

  private:
    PyObject* _pModule;
    PyObject* _pFunc;
    std::string _functionId;
    static std::map<std::string,PyObject*> _modules;
};

class Executor : public ParseAction{
  public:
    Executor(std::string code, const std::string& module);
    virtual void perform(Context& context);

  private:
    PyObject* _module;
    std::string _code;
    PyObject* _parsedCode;
};

}
#endif
