// contains some types and functions from python_integration which are also used elsewhere
// the bulk of python_integration is in python_integration.cpp

#ifndef PYTHON_INTEGRATION
#define PYTHON_INTEGRATION

#include <Python.h>
#include <map>
#include <set>
#include "../utils.hpp"

namespace parsing::python{

struct session;

using regSessType = std::map<std::pair<PyObject*,std::string>,session>;
using sessByTimeType = std::pair<timeType,session*>;
using sessByTimeSet = std::set<sessByTimeType>;

struct session {
    PyObject* session;
    timeType pointOfTimeout;
    timeType timeoutInterval;
    sessByTimeSet::iterator byTimeIt;
    regSessType::iterator registeredIt;
};

void endSession(regSessType::iterator sessionToEnd, const char* reason);
void timeoutSessions(timeType referenceTime);
PyMODINIT_FUNC PyInit_beaver(void);

}
#endif
