# provides a wrapper for session so that the class doesn't need to be defined using the python C API
# the class mostly just calls the underlying (standalone) C++ functions

import b

class session:

    @classmethod
    def get(cls,ident,*args,start_time=0,timeout=0,**kwargs):
        found = cls.find(ident)
        if(not found is None): return found
        instance = cls(*args,**kwargs)
        b.register(cls,ident,instance,start_time,timeout)
        return instance

    def register(self,ident,start_time=0,timeout=0):
        cls = type(self)
        b.register(cls,ident,self)

    @classmethod
    def terminate(cls,ident):
        b.end(cls,ident)

    @classmethod
    def find(cls,ident):
        return b.find(cls,ident)

    @classmethod
    def keep_alive(cls,ident,reference_time):
        b.keep_alive(cls,ident,reference_time)
