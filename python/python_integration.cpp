// implementation of python functions, compiled into beaver module

#include "python_integration.hpp"
#include "CallAction.hpp"
#include "../TreeStorage.hpp"
#include <Python.h>
#include <unordered_map>
#include <structmember.h>
#include <iostream>

namespace parsing::python{

regSessType registeredSessions;
sessByTimeSet sessionsByTime;

Py_hash_t hash(PyObject* obj) {
    auto ret = PyObject_Hash(obj);
    return ret;
}

static PyObject* session_register(PyObject* self, PyObject* args) {
    PyObject* sessionType = nullptr;
    const char* id = nullptr;
    PyObject* sessionInstance = nullptr;
    timeType beginTime = 0;
    timeType timeout = 0;
    if (!PyArg_ParseTuple(args,"OsOKK",&sessionType,&id,&sessionInstance,&beginTime,&timeout)) return nullptr;
    std::pair<PyObject*,std::string> key = {sessionType,std::string(id)};
    if(timeout > 0) timeout *= 1000000;
    session sess{sessionInstance,beginTime+timeout,timeout,sessionsByTime.end(),registeredSessions.end()};
    auto emplaceRes = registeredSessions.emplace(std::move(key),sess);
    if (emplaceRes.second) {
        emplaceRes.first->second.registeredIt = emplaceRes.first;
        Py_INCREF(sessionInstance);
        session& sref = emplaceRes.first->second;
        if (sref.timeoutInterval == 0) Py_RETURN_NONE;
        auto empRes2 = sessionsByTime.emplace(sref.pointOfTimeout,&sref);
        if (!empRes2.second)
            Warning::print("emplaced in registeredSessions but could not emplace in sessionsByTime, timeout won't work");
        sref.byTimeIt = empRes2.first;
    }
    Py_RETURN_NONE;
}

static PyObject* session_lookup(PyObject* self, PyObject* args) {
    PyObject* sessionType = nullptr;
    const char* id = nullptr;
    if (!PyArg_ParseTuple(args,"Os",&sessionType,&id)) return nullptr;
    std::pair<PyObject*,std::string> key = {sessionType,std::string(id)};
    auto found = registeredSessions.find(key);
    if (found == registeredSessions.end()) Py_RETURN_NONE;
    PyObject* ret = found->second.session;
    Py_INCREF(ret);
    return ret;
}

static PyObject* session_keepAlive(PyObject* self, PyObject* args) {
    PyObject* sessionType = nullptr;
    const char* id = nullptr;
    timeType referenceTime = 0;
    if (!PyArg_ParseTuple(args,"OsK",&sessionType,&id,&referenceTime)) return nullptr;
    auto found = registeredSessions.find({sessionType,id});
    if (found == registeredSessions.end()) Py_RETURN_NONE;
    session& sref = found->second;
    sref.pointOfTimeout += found->second.timeoutInterval;
    auto byTimeIt = sref.byTimeIt;
    sessionsByTime.erase(byTimeIt);
    auto emplaceRes = sessionsByTime.emplace(sref.pointOfTimeout,&sref);
    if (!emplaceRes.second) {
        Warning::print("could not re-emplace to sessionsByTime, timeout won't work");
        sref.byTimeIt = sessionsByTime.end();
    } else {
        sref.byTimeIt = emplaceRes.first;
    }
    Py_RETURN_NONE;
}

void endSession(regSessType::iterator sessionToEnd, const char* reason) {
    PyObject* session = sessionToEnd->second.session;
    PyObject* endMethod = PyObject_GetAttrString(session,"end");
    if (endMethod != nullptr) {
        Py_XDECREF(PyObject_CallFunction(endMethod,"s",reason));
        Py_DECREF(endMethod);
    } else {
        PyErr_Clear();
    }
    struct session& sess = sessionToEnd->second;
    auto byTimeIt = sess.byTimeIt;
    if (byTimeIt != sessionsByTime.end()) sessionsByTime.erase(byTimeIt);
    if (sessionToEnd == registeredSessions.end()) return;
    registeredSessions.erase(sessionToEnd);
    Py_DECREF(session);
}

static PyObject* session_end(PyObject* self, PyObject* args) {
    PyObject* cls = nullptr;
    const char* id = nullptr;
    if (!PyArg_ParseTuple(args,"Os",&cls,&id)) return nullptr;
    std::pair<PyObject*,std::string> key = {cls,std::string(id)};
    auto sessionIt = registeredSessions.find(key);
    if (sessionIt != registeredSessions.end()) {
        endSession(sessionIt,"manual");
    } else {
        Warning::print("session to end not found: \"",key.second.c_str(),"\"");
    }
    Py_RETURN_NONE;
}

void timeoutSessions(timeType referenceTime) {
    if (sessionsByTime.size() == 0) return;
    auto it = sessionsByTime.begin();
    while (it->first <= referenceTime && it != sessionsByTime.end()) {
        auto oldIt = it++;
        endSession(oldIt->second->registeredIt,"timeout");
    }
}

static void print(PyObject* string) {
    parsing::print(PyUnicode_AsUTF8(PyObject_Str(string)));
}

static PyObject* print(PyObject* self, PyObject* args) {
    PyObject* s = nullptr;
    if (!PyArg_ParseTuple(args,"O",&s)) return nullptr;
    print(s);
    Py_RETURN_NONE;
}

static PyObject* println(PyObject* self, PyObject* args) {
    PyObject* s = nullptr;
    if (!PyArg_ParseTuple(args,"|O",&s)) return nullptr;
    if (s != nullptr) print(s);
    parsing::println();
    Py_RETURN_NONE;
}

static PyObject* callTree(PyObject* self, PyObject* args) {
    PyObject* contextWrapper = nullptr;
    const char* treeName = nullptr;
    if (!PyArg_ParseTuple(args,"sO",&treeName,&contextWrapper)) return nullptr;
    Context* context = ((struct contextWrapper*)contextWrapper)->context;
    std::string treeId(treeName);
    ParseAction* tree = TreeStorage::find(treeId);
    if (tree == nullptr) {
        std::string errStr = "could not find tree "+treeId+"\"";
        PyErr_SetString(PyExc_RuntimeError,errStr.c_str());
        return nullptr;
    }
    tree->perform(*context);
    Py_RETURN_NONE;
}

static PyMethodDef integrationMethods[] = {
    {"register",(PyCFunction)session_register,METH_VARARGS,"register for timeout"},
    {"end",(PyCFunction)session_end,METH_VARARGS,"end a registered session"},
    {"find",(PyCFunction)session_lookup,METH_VARARGS,"lookup session, return None if not found"},
    {"keep_alive",(PyCFunction)session_keepAlive,METH_VARARGS,"postpone the timeout"},
    {"print",print,METH_VARARGS,"print argument(s) in a way consistent with other printing's buffering"},
    {"println",println,METH_VARARGS,"like print but prints only newline"},
    {"call_tree",callTree,METH_VARARGS,"parse given context by the specified (by name) tree"},
    {nullptr,nullptr,0,nullptr}
};

static struct PyModuleDef beaver = {
    PyModuleDef_HEAD_INIT,
    "beaver",
    nullptr,
    -1,
    integrationMethods
};

PyMODINIT_FUNC PyInit_beaver(void) {
    return PyModule_Create(&beaver);
}

}
