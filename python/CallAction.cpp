#include "CallAction.hpp"
#include "python_integration.hpp"
#include "../utils.hpp"
#include <structmember.h>
#include <iostream>

namespace parsing::python {

extern regSessType registeredSessions;

void init();
PyObject* getContextWrapper(Context* context);
void endAllSessions();

static PyObject* getAttribute(PyObject* self, PyObject* arg) {
    const char* attrName = PyUnicode_AsUTF8(arg);
    auto* context = ((contextWrapper*)self)->context;
    if (strcmp(attrName,"timestamp") == 0) {
        PyObject* ret = PyLong_FromUnsignedLongLong(context->getTimestamp());
        return ret;
    }
  str attribute = context->getAttribute(str(const_cast<char*>(attrName)));
  PyObject* ret = PyUnicode_FromStringAndSize(attribute.ptr,attribute.length);
  return ret;
}

static int setAttribute(PyObject* self, PyObject* attr, PyObject* value) {
    const char* attrName = PyUnicode_AsUTF8(attr);
    const char* attrValue = PyUnicode_AsUTF8(value);
    ((contextWrapper*)self)->context->addOrSetAttr(attrName,std::string(attrValue));
    return 0;
}

static PyObject* contextWrapper_new(PyTypeObject* type, PyObject*, PyObject*) {
  contextWrapper* ret = (contextWrapper*)type->tp_alloc(type,0);
  if (ret == nullptr) return nullptr;
  ret->context = nullptr;
  return (PyObject*) ret;
}

static PyMemberDef contextWrapperMembers[] = {{nullptr}};
static PyMethodDef contextWrapperMethods[] = {{nullptr}};

static PyTypeObject contextWrapperType = {
  PyVarObject_HEAD_INIT(nullptr,0)
  "contextWrapper",
  sizeof(contextWrapper),
    .tp_getattro = (getattrofunc)getAttribute,
    .tp_setattro = (setattrofunc)setAttribute,
    .tp_flags = Py_TPFLAGS_DEFAULT,
  .tp_methods = contextWrapperMethods,
  .tp_members = contextWrapperMembers,
};

void init() {
    static bool initialized = false;
    if (initialized) return;
    PyImport_AppendInittab("b",&PyInit_beaver);
    Py_Initialize();
    if (PyType_Ready(&contextWrapperType) != 0) throw std::runtime_error("could not initialize contextWrapper type");
    initialized = true;
}

PyObject* getContextWrapper(Context* context) {
    PyObject* ret = contextWrapper_new(&contextWrapperType,nullptr,nullptr);
    ((contextWrapper*)ret)->context = context;
    return ret;
}

void endAllSessions() {
    auto sessionIt = registeredSessions.begin();
    while (sessionIt != registeredSessions.end()) {
        endSession(sessionIt++,"end");
    }
}

static PyObject* wrapper = nullptr;

//class CallAction

CallAction::CallAction(const std::string& functionId, const std::string& defaultModule):_functionId(functionId) {
    int dotIndex = -1;
    for (unsigned int i=0; i<functionId.size(); i++) {
        if (functionId[i] == '.') dotIndex = i;
    }
    std::string moduleName = dotIndex<0 ? defaultModule : std::string(functionId,0,dotIndex);
    const char* functionName = functionId.c_str()+dotIndex+1;
    _pModule = getModule(moduleName);
    if (_pModule != nullptr) {
        _pFunc = PyObject_GetAttrString(_pModule,functionName);
    } else {//unable to import
        _pFunc = nullptr;
    }
    if (_pFunc == nullptr) {
        throw std::runtime_error("could not initialize call to "+functionId+(_pModule==nullptr ? " nor its module" : ""));
    }
}

CallAction::~CallAction() = default;

bool CallAction::isValid() {
    return _pModule != nullptr && _pFunc != nullptr;
}

PyObject* CallAction::getModule(const std::string& moduleName) {
    init();
    if (wrapper == nullptr) wrapper = getContextWrapper(nullptr);
    auto moduleIt = _modules.find(moduleName);
    if (moduleIt == _modules.end()) {//new module, import it
        auto module = PyImport_ImportModule(moduleName.c_str());
        if (module == nullptr) {
            PyErr_Print();
            throw std::runtime_error("could not import module "+moduleName);
        }
        Py_INCREF(wrapper);
        if (PyModule_AddObject(module,"c",wrapper) != 0) {
            Py_DECREF(wrapper);
            throw std::runtime_error("could not add c to python module");
        }
        _modules.emplace(std::move(moduleName),module);
        return module;
    } else {//module was already imported
        return moduleIt->second;
    }
}

void CallAction::perform(Context& context) {
    if (_pModule == nullptr) throw std::runtime_error("trying to perform invalid CallAction");
    ((contextWrapper*)wrapper)->context = &context;
    _debug.print("calling function \"",_functionId.c_str(),"\"");
    PyObject* userRet = PyObject_CallFunction(_pFunc,nullptr);
    Py_XDECREF(userRet);
    bool callFailed = userRet == nullptr;
    if (callFailed) {
        Warning::printWithInput(&context.getOriginalMsg(),"call to ",_functionId.c_str()," failed");
        PyErr_Print();
    }
}

std::map<std::string,PyObject*> CallAction::_modules;

//class Executor

Executor::Executor(std::string code, const std::string& module) : _code(code) {
    Py_Initialize();
    _module = CallAction::getModule(module);
    _parsedCode = Py_CompileString(_code.c_str(),"inline python",Py_file_input);
    if (_parsedCode == nullptr) throw std::runtime_error("inline python did not compile, code:"+_code);
}

void Executor::perform(Context& context) {
    ((contextWrapper*)wrapper)->context = &context;
    PyObject* globals = PyModule_GetDict(_module);
    if (_debug.isEnabled()) _debug.print("executing the following code:",_code.c_str(),"<end of code>\n");
    if (PyEval_EvalCode(_parsedCode,globals,nullptr) == nullptr) {
        PyErr_Print();
    }
}
}
