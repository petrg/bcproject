// parses configuration files and builds trees from them

#ifndef CONFIG_PARSER
#define CONFIG_PARSER

#include <iostream>
#include <memory>
#include <map>
#include <unordered_set>
#include <fstream>
#include <vector>
#include <functional>
#include "ParseAction.hpp"
#include "MatchOption.hpp"
#include "python/CallAction.hpp"
#include "TreeStorage.hpp"

namespace parsing {
bool isFinal(const std::string& attributeName);

class ConfigParser {
  public:
    struct options {
        std::string readFrom = "";
        bool openSocket = false;
        std::string sortOrder = "";
        bool implicitMonitor = false;
        std::string progressStorage = "";
    };
    ConfigParser(std::string&& filename);
    void parse(bool mainConfig = true);
    const options& getOptions();
    static void check();
    static void enableDebug();
    static void setBaseDir(std::string dir);
    static const std::string& getBaseDir();

  private:
    using treeId = std::string;
    using treeToFillT = std::pair<std::vector<ParseAction*>&,size_t>;
    struct filePos {
        std::string fileName;
        std::pair<unsigned int, unsigned int> position;
    };
    struct treePlacement {
        std::vector<treeToFillT> places;
        std::vector<filePos> filePositions;
    };

    class Lexer {
      public:
        Lexer(const std::string& fileName);
        Lexer& operator>>(std::string& dest);
        Lexer& operator>>(char& dest);
        void readWord(std::string& dest, bool skipNewlines=true);
        void skipSpaces(bool includingNewlines=true);
        bool eof(bool disableComments=false);
        bool good() const;
        int peek(bool disableComments=false);
        int get(bool disableComments=false);
        std::pair<unsigned int,unsigned int> getFilePos() const;
        std::string readToTermination(char terminationChar, ConfigParser* caller);

      private:
        std::ifstream _input;
        unsigned int _lineNum;
        unsigned int _charNum;
    };

    std::string _fileName;
    Lexer _input;
    std::vector<std::string> _defaultModule;
    std::string _keyword;
    std::map<std::string,std::function<void(std::vector<ParseAction*>&)>> _parseFunctions;
    std::vector<std::string> _patternFiles;
    options _options;
    static std::unordered_set<std::string> _parsedFiles;
    static std::map<treeId,treePlacement> _treesToFill;
    static DebugPrinter _debug;
    static std::string _baseDir;

    static std::string resolvePath(const std::string& path);
    void parseBlock(const std::map<std::string,std::function<void()>>& keywordProcessors);
    void checkNoInput();
    void parseInputBlock();
    void parseSocketSpec();
    void parseElement();
    ParseAction* parseTree();
    ParseAction* parseMatch();
    void parseActionList(std::vector<ParseAction*>& actions);
    std::unique_ptr<MatchOption> parseOption();
    ParseAction* parseCall();
    ParseAction* parseExec();
    ParseAction* parsePrint();
    void parseExternalFile(std::string&& name);
    void findExternalTree(treeToFillT placeToFill);
    ParseAction* parseTimeFormat();
    void reportError(std::string expected, std::string found);
    void reportGeneralError(std::string msg);
    void importPatterns();
};
}
#endif
