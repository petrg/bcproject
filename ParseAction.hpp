#ifndef PARSE_ACTION
#define PARSE_ACTION
#include <vector>
#include "Context.hpp"
#include "MatchOption.hpp"

namespace parsing {

struct MatchOption;

class ParseAction {
  public:
    virtual void perform(Context& context) = 0;
    virtual ~ParseAction() = default;
    static ParseAction* storeAction(std::unique_ptr<ParseAction> action);
    static void enableDebug();
  protected:
    static DebugPrinter _debug;
  private:
    static std::vector<std::unique_ptr<ParseAction>> _actionOwner;
};

class Root : public ParseAction {
  public:
    explicit Root(std::string treeName);
    Root(std::string treeName, std::vector<ParseAction*> actions);
    static std::pair<Root*,std::vector<ParseAction*>&> create(std::string treeName);//creates an instance and returns also its _actions
    void perform(Context& context) override;
    void addAction(ParseAction* action);

  private:
    std::vector<ParseAction*> _actions;
    std::string _treeName;
};

class Match : public ParseAction {
  public:
    Match(std::string subjectName,int subjectIndex, bool subjectIsFinal);
    void perform(Context& context) override;
    MatchOption& addMatchOption(std::unique_ptr<MatchOption> option);
    void setBreakFlag();
    static bool extractBreakFlag();

  private:
    static bool _breakFlag;
    std::vector<std::unique_ptr<MatchOption>> _options;
    int _subjectIndex;
    bool _subjectIsFinal;
    std::string _subjectName;
    bool _instanceBreakFlag;
    static bool _classBreakFlag;
};

class TimestampParser : public ParseAction {
  public:
    TimestampParser(std::string&& format, const std::string& attribute, const std::string& attribute2,
                    const std::string& offsetAttr);
    void perform(Context& context) override;

  private:
    std::string _format;
    std::pair<int,int> _attrIndexes;
    std::pair<bool,bool> _attrsFinal;
    int _offsetAttrIndex;
    bool _offsetAttrFinal;
    struct tm _lastTime;
    timeType _lastTimestamp;
};

class Print : public ParseAction {
  public:
    explicit Print(std::string firstLiteral);
    Print() = default;
    void addLiteral(std::string literal);
    void addAttributeToPrint(int index, bool isFinal);
    void perform(Context& context) override;

  private:
    std::vector<std::string> _literals;
    std::vector<std::pair<int,bool>> _attributes;
};

class ContextPrint : public ParseAction {
  public:
    void perform(Context& context) override;
};
}
#endif
