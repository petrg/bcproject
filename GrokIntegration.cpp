#include <iostream>
#include <stdio.h>
#include "Context.hpp"
#include "GrokIntegration.hpp"
#include "MatchOption.hpp"

namespace parsing {

GrokIntegration::GrokIntegration(std::string&& pattern, const std::vector<std::string>& patternFiles) : _pattern(std::move(pattern)) {
    grok_init(&_grok);
    for (const auto& patternFile : patternFiles) {
        if (grok_patterns_import_from_file(&_grok,patternFile.c_str()) != 0) {
            throw std::runtime_error("could not import patterns from \""+patternFile+"\"");
        }
    }
    grok_compile(&_grok,_pattern.c_str());
    if (_grok.re == NULL || _grok.re == nullptr) {
        throw std::runtime_error("pattern \""+_pattern+"\" did not grok_compile");
    }
    grok_capture_walk_init(&_grok);
    while (true) {
        auto* capture = grok_capture_walk_next(&_grok);
        if (capture == nullptr) break;
        _captures.push_back(capture);
    }
}

GrokIntegration::~GrokIntegration() {
    grok_free(&_grok);
}

bool GrokIntegration::tryMatch(str subject, Context& context, MatchOption& option) const {
    struct grok_match match;
    if (!tryMatch(subject,match)) return false;
    int walkIndex = 0;
    for (auto* capture : _captures) {
        auto start = match.grok->pcre_capture_vector[capture->pcre_capture_number*2];
        auto end = match.grok->pcre_capture_vector[capture->pcre_capture_number*2+1];
        str value(const_cast<char*>(match.subject+start),end-start);
        auto matchedPartId = option.attributeIndexes[walkIndex++];
        context.setAttribute(matchedPartId.first, matchedPartId.second, value);
    }
    return true;
}

bool GrokIntegration::matches(str subject) const {
    struct grok_match match;
    return tryMatch(subject,match);
}

std::vector<str> GrokIntegration::getAttributeNames() const {
    std::vector<str> ret;
    ret.reserve(_captures.size());
    grok_capture_walk_init(&_grok);
    const grok_capture* capture;
    for (const auto* capture : _captures) {
        str captureName(capture->name,capture->name_len);
        ret.push_back(captureName);
    }
    return ret;
}

const std::string& GrokIntegration::getPattern() {
    return _pattern;
}

bool GrokIntegration::tryMatch(str subject, struct grok_match& match) const {
    match.subject = nullptr;
    grok_execn(&_grok,subject.ptr,subject.length,&match);
    return match.subject != nullptr;
}

}
