#include "ConfigParser.hpp"
#include <fstream>
#include <libgen.h>

namespace parsing {

//class ConfigParser

ConfigParser::ConfigParser(std::string&& filename) : _fileName(std::move(filename)),_input(resolvePath(_fileName)),_defaultModule() {
    _defaultModule.push_back("empty");
    _parseFunctions = {
        {"match:", [this](auto& actions) {actions.push_back(parseMatch());}},
        {"call:", [this](auto& actions) {actions.push_back(parseCall());}},
        {"execute", [this](auto& actions) {actions.push_back(parseExec());}},
        {"default_module:", [this](auto&) {std::string module; _input.readWord(module,false); _defaultModule.push_back(std::move(module));}},
        {"parse_by:", [this](auto& actions) {
            actions.push_back(nullptr);
            findExternalTree(treeToFillT(actions,actions.size()-1));
        }},
        {"timestamp:", [this](auto& actions) {actions.push_back(parseTimeFormat());}},
        {"print:", [this](auto& actions) {actions.push_back(parsePrint());}},
    };
}

void ConfigParser::parse(bool mainConfig) {
    _debug.incIndent();
    _debug.print("starting parsing of file \"",_fileName.c_str(),"\"");
    _parsedFiles.emplace(_fileName);
    _input >> _keyword;
    while (!_input.eof()) parseElement();
    if (mainConfig && TreeStorage::find(_fileName+":main") == nullptr)
        reportGeneralError("no main tree found in main config file \""+_fileName+"\"");
    _debug.print("finished parsing of file \"",_fileName.c_str(),"\"");
    _debug.decIndent();
}

const ConfigParser::options& ConfigParser::getOptions() {
    return _options;
}

void ConfigParser::check() {
    if (_treesToFill.size() == 0) return;
    for (const auto& tree : _treesToFill) {
        std::cerr << "unknown named tree: \""+tree.first+"\"";
        bool first = true;
        for (const auto& pos : tree.second.filePositions) {
            if (!first) std::cerr << ",";
            first = false;
            std:: cerr << " " << pos.fileName << ":" << pos.position.first << ":" << pos.position.second;
        }
        std::cerr << std::endl;
    }
    throw std::runtime_error("aborting due to unknown named trees");
}

void ConfigParser::enableDebug() {
    _debug.enable();
}

void ConfigParser::setBaseDir(std::string dir) {
    _baseDir = dir;
}

const std::string& ConfigParser::getBaseDir() {
    return _baseDir;
}

//class ConfigParser::Lexer

ConfigParser::Lexer::Lexer(const std::string& fileName):_input(fileName),_lineNum(1),_charNum(1) {
    if (!_input.good()) throw std::runtime_error("could not open file "+fileName);
}
ConfigParser::Lexer& ConfigParser::Lexer::operator>>(std::string& dest) {
    readWord(dest);
    return *this;
}

ConfigParser::Lexer& ConfigParser::Lexer::operator>>(char& dest) {
    skipSpaces();
    dest = 0;
    if (!eof()) dest = (char)get();
    return *this;
}

void ConfigParser::Lexer::readWord(std::string& dest, bool skipNewlines) {
    dest.erase();
    skipSpaces(skipNewlines);
    if (peek() == '{' || peek() == '}') {
        dest.append(1,(char)get());
        return;
    }
    while (!eof() && !std::isspace(peek()) && peek() != '{' && peek() != '}') dest.append(1,(char)get());
}

void ConfigParser::Lexer::skipSpaces(bool includingNewlines) {
    char c = peek();
    while (!eof() && std::isspace(c) && (includingNewlines || c != '\n')) {
        get();
        c = peek();
    }
}

bool ConfigParser::Lexer::eof(bool disableComments) {
    peek(disableComments);
    return _input.eof();
}

//bool ConfigParser::Lexer::good() const {
//    return _input.good();
//}

int ConfigParser::Lexer::peek(bool disableComments) {
    auto ret = _input.peek();
    if (ret == '#' && !disableComments) {
        get(true);
        _debug.print("reading to end of line");
        std::string readStr;
        while (peek(true) != '\n') readStr.append(1,get(true));
        _debug.print("read to end of line, read string: \"",readStr.c_str(),"\"");
        ret = _input.peek();
    }
    return ret;
}

int ConfigParser::Lexer::get(bool disableComments) {
    if (!disableComments) peek(false);//skips comments if there are any
    if (!eof(disableComments)) _charNum++;
    char ret = (char)_input.get();
    if (ret == '\n') {
        _lineNum++;
        _charNum = 1;
    }
    return ret;
}

std::pair<unsigned int,unsigned int> ConfigParser::Lexer::getFilePos() const {
    return {_lineNum,_charNum};
}

std::string ConfigParser::Lexer::readToTermination(char terminatingChar,ConfigParser* caller) {
    if (_debug.isEnabled()) {
        char terminator[2];
        terminator[0] = terminatingChar;
        terminator[1] = '\0';
        _debug.print("reading to termination by char '",terminator,"'");
    }
    std::string ret;
    char c;
    bool escaped = false;
    while ((c = (char) get(true)) != terminatingChar || escaped) {
        if (eof(true)) {
            std::string errMsg = "reached eof before terminating char '";
            errMsg.append(1,terminatingChar);
            errMsg.append("'");
            caller->reportGeneralError(std::move(errMsg));
        }
        ret.append(1,c);
        escaped = c == '\\' && !escaped;
    }
    _debug.print("termination encountered, read \"",ret.c_str(),"\"");
    return ret;
}

//class ConfigParser (continued)

std::string ConfigParser::resolvePath(const std::string& path) {
    if (path[0] == '/') return path;
    return _baseDir+"/"+path;
}

void ConfigParser::parseBlock(const std::map<std::string,std::function<void()>>& keywordProcessors) {
        if (_keyword != "{") reportError("{",_keyword);
        do {
            _input >> _keyword;
            auto processor = keywordProcessors.find(_keyword);
            if (processor == keywordProcessors.end()) break;
            processor->second();
        } while (_keyword != "}");
        if (_keyword != "}") reportError("}",_keyword);
}

void ConfigParser::checkNoInput() {
    if (!_options.readFrom.empty()) {
        reportGeneralError("encountered multiple input specifications,"
        "only one \"input:\" and only one \"open_socket:\" and not both can be present in the config");
    }
}

void ConfigParser::parseInputBlock() {
    checkNoInput();
    std::map<std::string,std::function<void()>> optionsProcessors = {
        {"path:", [this]() {_input.readWord(_options.readFrom,false);}},
        {"sort:", [this]() {_input.readWord(_options.sortOrder,false);
            if (_options.sortOrder != "lex" && _options.sortOrder != "num") reportGeneralError("sort order must be lex or num");}},
        {"progress_storage:", [this]() {_input.readWord(_options.progressStorage,false);}},
        {"monitor", [this]() {_options.implicitMonitor = true;}},
    };
    _input.readWord(_keyword,false);
    _debug.print("detected input:");
    parseBlock(optionsProcessors);
    _input >> _keyword;
}

void ConfigParser::parseSocketSpec() {
    checkNoInput();
    _input.readWord(_options.readFrom,false);
    _options.openSocket = true;
    _input >> _keyword;
}

void ConfigParser::parseElement() {
    std::map<std::string,std::function<void()>> specialParsers = {
        {"input:",[this](){parseInputBlock();}},
        {"open_socket:",[this](){parseSocketSpec();}},
    };
    auto parserIt = specialParsers.find(_keyword);
    if (parserIt == specialParsers.end()) {
        parseTree();
    } else {
        parserIt->second();
    }
}

ParseAction* ConfigParser::parseTree() {
    std::string name = "main";
    bool treeWasNamed = false;
    if (_parseFunctions.find(_keyword) == _parseFunctions.end()) {
        treeWasNamed = true;
        name = std::move(_keyword);
        _debug.print("detected tree name \"",name.c_str(),"\"");
        _input >> _keyword;
        if (_keyword != "{") reportError("{",_keyword);
        _input >> _keyword;
    }
    std::string treeId = _fileName+":"+name;
    auto rootActionsPair = Root::create(treeId);
    Root* tree = rootActionsPair.first;
    std::vector<ParseAction*>& actions = rootActionsPair.second;
    parseActionList(actions);
    auto fillIt = _treesToFill.find(treeId);
    auto emplaceRes = TreeStorage::add(std::move(treeId),tree);
    if (!emplaceRes.second) throw std::runtime_error("duplicate tree name: "+name);
    if (fillIt != _treesToFill.end()) {
        auto& placesToFill = fillIt->second.places;
        ParseAction* filledTree = emplaceRes.first->second;
        for (treeToFillT placeToFill : placesToFill) {
            auto& vec = placeToFill.first;
            auto index = placeToFill.second;
            auto& ref = vec[index];
            ref = filledTree;
        }
        _treesToFill.erase(fillIt);
    }
    if (treeWasNamed) {
        _debug.print("looking for closing brace after tree declaration");
        if (_keyword != "}") reportError("}",_keyword);
        _input >> _keyword;
    }
    return tree;
}

ParseAction* ConfigParser::parseMatch() {
    std::string subjectName;
    _input.readWord(subjectName,false);
    _debug.incIndent();
    _debug.print("parsing match, subject = \"",subjectName.c_str(),"\"");
    if (subjectName.find(":") != std::string::npos) reportGeneralError("attribute names cannot contain \":\"");
    bool subjectIsFinal = Context::isFinal(str(subjectName));
    int subjectIndex = Context::addAttribute(subjectName);
    auto ret = std::make_unique<Match>(std::move(subjectName),subjectIndex,subjectIsFinal);
    _input >> _keyword;
    auto patternFileCount = _patternFiles.size();
    if (_keyword != "{") reportError("{",_keyword);
    _input >> _keyword;
    if (_keyword == "import_patterns:") {
        importPatterns();
        _input >> _keyword;
    }
    while (_keyword != "}") {
        _debug.print("keyword = \"",_keyword.c_str(),"\", about to parse option");
        ret->addMatchOption(parseOption());
    }
    _input >> _keyword;
    if (_keyword == "break") {
        ret->setBreakFlag();
        _debug.print("found break after match");
        _input >> _keyword;
    }
    while (_patternFiles.size() > patternFileCount) _patternFiles.pop_back();//pattern files are scoped to the subtree
    _debug.print("keyword = \"",_keyword.c_str(),"\", done parsing match");
    _debug.decIndent();
    return ParseAction::storeAction(std::move(ret));
}

void ConfigParser::parseActionList(std::vector<ParseAction*>& actions) {
    auto func = _parseFunctions.end();
    while ((func = _parseFunctions.find(_keyword)) != _parseFunctions.end()) {
        bool readNext = _keyword != "match:";
        (func->second)(actions);
        if (readNext) _input >> _keyword;
    }
}

std::unique_ptr<MatchOption> ConfigParser::parseOption() {
    _debug.incIndent();
    _debug.print("parsing option");
    if (_keyword != "{") reportError("{",_keyword);
    std::string pattern = "";
    _debug.print("about to read the pattern");
    char c;
    _input >> c;
    if (c != '"') {
        std::string found;
        found.append(1,c);
        reportError("\"",found);
    }
    pattern = _input.readToTermination('\"',this);
    auto ret = std::make_unique<MatchOption>(std::move(pattern),_patternFiles);
    auto attributeNames = ret->grok.getAttributeNames();
    for (str attributeName : attributeNames) {
        std::string attrNameStr = toString(attributeName);
        auto colonIndex = attrNameStr.find(":");
        if (colonIndex != std::string::npos) {//generated by grok's %{pattern:attribute}, need to isolate attribute
            attrNameStr.erase(0,colonIndex+1);
            attributeName = str(attrNameStr);
        }
        bool attributeIsFinal = Context::isFinal(attributeName);
        int attributeIndex = Context::addAttribute(std::move(attrNameStr));
        ret->attributeIndexes.emplace_back(attributeIndex,attributeIsFinal);
    }
    _input >> _keyword;
    auto moduleLevel = _defaultModule.size();
    parseActionList(ret->actions);
    while (_defaultModule.size() > moduleLevel) {
        //default modules are scoped to the subtree, when leaving recursion, they should be poped to the original state
        _defaultModule.pop_back();
    }
    _debug.print("finishing parsing of option, looking for }");
    if (_keyword != "}") reportError("}",_keyword);
    _input >> _keyword;
    if (_keyword == "break") {
        _debug.print("detected break after option, setting its breakFlag");
        ret->breakFlag = true;
        _input >> _keyword;
    }
    _debug.decIndent();
    return ret;
}

ParseAction* ConfigParser::parseCall() {
    _debug.incIndent();
    _debug.print("parsing call");
    parsing::python::init();
    std::string functionName;
    _input.readWord(functionName,false);
    _debug.print("function name = \"",functionName.c_str(),"\"");
    if (functionName.empty()) reportGeneralError("function name empty");
    parsing::python::CallAction ret(functionName,_defaultModule.back());
    if (!ret.isValid()) throw std::runtime_error("could not properly initialize call to "+functionName);
    _debug.print("finished parsing call");
    _debug.decIndent();
    return ParseAction::storeAction(std::make_unique<python::CallAction>(std::move(ret)));
}

ParseAction* ConfigParser::parseExec() {
    _debug.incIndent();
    _debug.print("parsing exec");
    parsing::python::init();
    _input >> _keyword;
    std::string moduleName = _defaultModule.back();
    bool except = false;
    if (_keyword != "{") {
        if (_keyword == "in") {
            _debug.print("found \"in\" keyword, looking for module name");
            _input >> _keyword;
            moduleName = _keyword;
            std::string skipped;
            _input >> skipped;
            if (skipped != "{") except = true;
        } else {
            except = true;
        }
        if (except) reportGeneralError("expected \"{\" and python code, found \""+_keyword+"\"");
    }
    _debug.print("skipping to the next line");
    _input.skipSpaces(false);
    if (_input.peek() != '\n') reportGeneralError("code in execute block must begin on a new line");
    _debug.print("reading code");
    std::string code = _input.readToTermination('}',this);
    int indentBegin=-1 ,indentEnd=0;
    for (int i=0; i<code.size(); i++) {//figure out basic indentation level as the indent level of the first line containing non-spaces
        if (code[i] == '\n') indentBegin = i;
        if (!std::isspace(code[i])) {
            indentEnd = i;
            break;
        }
    }
    int indent = indentEnd-indentBegin-1;
    auto filePos = _input.getFilePos();

    //closing brace can appear in the python code, waiting for one which is indented less than the rest of the code
    while (filePos.second > indent) {
        code.append(1,'}');
        code.append(_input.readToTermination('}',this));
    }
    _debug.print("inferred indentation level is ",std::to_string(indent).c_str()," whitespaces");
    std::string indentedCode;
    for (int i=indentBegin; i<code.size(); i++) {//subtract the indentation level from all the rest of the code
        if (i<0 || code[i] == '\n') {
            if (code[i] == '\n') indentedCode.append(1,'\n');
            int j;
            for (j=i+1; j<i+indent+1 && j<code.size(); j++) {//skip the next <indentation_level> whitespaces
                if (!std::isspace(code[j])) {
                    reportGeneralError("indentation error, expected spaces, got \""+std::string(code,j)+"\"");
                }
            }
            i = j;
        }
        indentedCode.append(1,code[i]);
    }
    _debug.print("done parsing exec");
    _debug.decIndent();
    return ParseAction::storeAction(std::make_unique<python::Executor>(std::move(indentedCode),moduleName));
}

ParseAction* ConfigParser::parsePrint() {
    _debug.incIndent();
    _debug.print("parsing print");
    _input.skipSpaces(false);
    if (_input.peek() == '"') {
        _input.get();
        std::string printStatement = _input.readToTermination('"',this);
        auto bracePos = printStatement.find("{");
        if (bracePos == std::string::npos) {
            _debug.print("found no opening brace, no substitution done");
            _debug.decIndent();
            return ParseAction::storeAction(std::make_unique<Print>(std::move(printStatement)));
        }
        auto ret = std::make_unique<Print>();
        std::string literal;
        while ((bracePos = printStatement.find("{")) != std::string::npos) {
            _debug.print("next opening brace at pos ",std::to_string(bracePos).c_str(),
                " preceding literal: \"",printStatement.substr(0,bracePos).c_str(),"\"");
            auto closing = printStatement.find("}",bracePos);
            if (closing == std::string::npos) reportGeneralError("missing a } in a print statement");
            _debug.print("closing brace at pos ",std::to_string(closing).c_str());
            literal.append(printStatement.substr(0,bracePos));
            printStatement.erase(0,bracePos);
            _debug.print("erased literal, got \"",printStatement.c_str(),"\"");
            std::string attrName = printStatement.substr(1,closing-bracePos-1);
            printStatement.erase(0,closing-bracePos+1);
            _debug.print("erased attribute name, got \"",printStatement.c_str(),"\"");

            if (attrName[0] == '&') {
                std::map<std::string,std::string> supportedEscapes = {
                    {"quote","\""},
                    {"lbrace","{"},
                    {"rbrace","}"},
                };
                attrName.erase(0,1);
                auto escapedIt = supportedEscapes.find(attrName);
                if (escapedIt != supportedEscapes.end()) {
                    _debug.print("detected the following escape sequence: \"",attrName.c_str(),"\"");
                    literal.append(escapedIt->second);
                } else {
                    reportGeneralError("unknown escape sequence: \""+attrName+"\"");
                }
            } else {
                _debug.print("attribute name \"",attrName.c_str(),"\"");
                ret->addLiteral(std::move(literal));
                bool attrFinal = Context::isFinal(attrName);
                auto attrIndex = Context::addAttribute(std::move(attrName));
                ret->addAttributeToPrint(attrIndex,attrFinal);
                literal.clear();
            }
        }
        _debug.print("no more attributes detected, leftover is \"",printStatement.c_str(),"\"");
        literal.append(printStatement);
        ret->addLiteral(std::move(literal));
        _debug.print("finished parsing print");
        _debug.decIndent();
        return ParseAction::storeAction(std::move(ret));
    } else {
        _input.readWord(_keyword,false);
        if (_keyword == "context") {
            return ParseAction::storeAction(std::make_unique<ContextPrint>());
        } else {
            reportError("\"",_keyword);//throws
            throw std::runtime_error("reached code which should be unreachable");
        }
        _debug.decIndent();
    }
}

void ConfigParser::parseExternalFile(std::string&& name) {
    ConfigParser parser(std::move(name));
    parser.parse(false);
}

void ConfigParser::findExternalTree(treeToFillT placeToFill) {
    _debug.incIndent();
    _debug.print("parsing external tree reference");
    _input >> _keyword;
    std::string fileName;
    std::string treeName;
    auto separatorPos = _keyword.find(":");
    if (separatorPos == std::string::npos) {
        treeName = _fileName+":"+_keyword;
    } else {
        fileName = _keyword.substr(0,separatorPos);
        treeName = std::move(_keyword);
    }
    _debug.print("filename = \"",fileName.c_str(),"\"","treeName = \"",treeName.c_str(),"\"");
    if (!fileName.empty() && _parsedFiles.find(fileName) == _parsedFiles.end()) {
        parseExternalFile(std::move(fileName));
    }
    auto* tree = TreeStorage::find(treeName);
    if (tree == nullptr) {
        _debug.print("tree \"",treeName.c_str(),"\" is not known, adding to list of trees to fill later");
        auto treePlacementIt = _treesToFill.find(treeName);
        if (treePlacementIt == _treesToFill.end()) {
            treePlacement places;
            places.filePositions.push_back({_fileName,_input.getFilePos()});
            treePlacementIt = _treesToFill.emplace(std::move(treeName),std::move(places)).first;
        }
        auto& places = treePlacementIt->second.places;
        places.push_back(placeToFill);
    } else {
        placeToFill.first[placeToFill.second] = tree;
    }
    _debug.print("done parsing external tree reference");
    _debug.decIndent();
}

ParseAction* ConfigParser::parseTimeFormat() {
   _debug.incIndent();
   _debug.print("parsing time format specification");
   struct  {
       std::string format = "";
       std::string attr = "";
       std::string microsAttr = "";
       std::string zoneAttr = "";
   } options;
   std::map<std::string,std::function<void()>> processors = {
       {"format:", [&options,this]() {
           _input.skipSpaces();
           char c = (char)_input.get();
           if (c != '"') {
               std::string err;
               err.append(1,c);
               reportError("\"",err);
           }
           options.format = _input.readToTermination('"',this);
       }},
       {"from:", [&options,this]() {_input.readWord(options.attr);}},
       {"micros_from:", [&options,this]() {_input.readWord(options.microsAttr,false);}},
       {"zone_from:", [&options,this]() {_input.readWord(options.zoneAttr,false);}},
   };
   _input >> _keyword;
   parseBlock(processors);
   auto ret = std::make_unique<TimestampParser>(std::move(options.format),options.attr,options.microsAttr,options.zoneAttr);
   return ParseAction::storeAction(std::move(ret));
}

void ConfigParser::reportError(std::string expected, std::string found) {
    reportGeneralError("expected \""+expected+"\", found \""+found+"\"");
}

void ConfigParser::reportGeneralError(std::string msg) {
    auto filePos = _input.getFilePos();
    throw std::runtime_error(_fileName+":"+std::to_string(filePos.first)+":"+std::to_string(filePos.second)+": "+std::move(msg));
}

void ConfigParser::importPatterns() {
    _debug.incIndent();
    _debug.print("parsing names of patterns to import");
    std::string name;
    while (true) {
        _input.skipSpaces(false);
        if (_input.peek() == '"') {
            _input.get();
            name = _input.readToTermination('"',this);
        } else {
            _input.readWord(name,false);
        }
        if (name.empty()) break;
        _debug.print( "found pattern file name \"",name.c_str(),"\"");
        _patternFiles.push_back(resolvePath(name));
    }
    _debug.decIndent();
}

std::unordered_set<std::string> ConfigParser::_parsedFiles;
std::map<ConfigParser::treeId,ConfigParser::treePlacement> ConfigParser::_treesToFill;
DebugPrinter ConfigParser::_debug;
std::string ConfigParser::_baseDir;
// } //class ConfigParser
}
