// integration layer with grok offering constructor, destructor and hiding that grok is written in C
// an instance of GrokIntegration represents one regular expression

#ifndef GROK_INTEGRATION
#define GROK_INTEGRATION
extern "C"{
#include <grok.h>
}

#include <list>
#include <vector>
#include "utils.hpp"
#include "Context.hpp"

namespace parsing {

class MatchOption;

class GrokIntegration {
  public:
    GrokIntegration(std::string&& pattern, const std::vector<std::string>& patternFiles);
    ~GrokIntegration();
    bool tryMatch(str subject, Context& context, MatchOption& option) const;
    bool matches(str subject) const;
    std::vector<str> getAttributeNames() const;
    const std::string& getPattern();

  private:
    std::string _pattern;
    struct grok _grok;
    std::vector<const grok_capture*> _captures;
    bool tryMatch(str subject,struct grok_match& match) const;
};

}
#endif
