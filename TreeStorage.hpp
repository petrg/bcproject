// stores trees created by ConfigParser by name (the name includes the name of the configuration file the tree is taken from)
// trees stored here can be accessed both by ConfigParser and python integration, which enables user scripts to use parse_by

#ifndef TREE_STORAGE
#define TREE_STORAGE

#include "ParseAction.hpp"

namespace parsing {
using treeId = std::string;

class TreeStorage {
    public:
        TreeStorage() = delete;
        static ParseAction* find(treeId id) {
            auto found = _namedTrees.find(id);
            if (found == _namedTrees.end()) return nullptr;
            return found->second;
        }
        static auto add(treeId id, ParseAction* value) {
            return _namedTrees.emplace(std::move(id),value);
        }

    private:
        static std::map<treeId,ParseAction*> _namedTrees;
};
}
#endif
