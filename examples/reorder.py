import session
import b

class session(session.session):
    def __init__(self,msg_id):
        self.msg_id = msg_id
        self.messages = []
    def addMsg(self,msg):
        self.messages.append(msg)
    def end(self,reason):
        for msg in self.messages:
            b.println(msg)
        b.println()

def process_msg():
    inst = session.get(c.msg_id,msg_id=c.msg_id,start_time=c.timestamp,timeout=100)
    session.keep_alive(c.msg_id,c.timestamp)
    inst.messages.append(c.msg)
    if(c.message == "removed"): session.terminate(c.msg_id)
