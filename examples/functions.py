import session
import b

class session(session.session):
    def __init__(self,time,innerident):
        self.time = time
        self.ident = innerident
        self.counter = 0
    def end(self,reason):
#        b.print("session ")
#        b.print(self.ident)
#        b.print(" lasted from ")
#        b.print(self.time)
#        b.print(" to present, terminated for reason: ")
#        b.print(reason)
#        b.println()
        b.println(f"session {self.ident} lasted from {self.time} to present, terminated for reason {reason}")
    def inc(self):
        self.counter += 1
    def dec(self):
        self.counter -= 1

def initSession():
    ident = c.ID
    timestamp = c._for_strptime
    session.get(ident,start_time=c.timestamp,timeout=100,innerident=ident,time=timestamp).inc()

def endSession():
    ident = c.ID
    s = session.find(ident)
    if(s != None):
        s.dec()
        if(s.counter == 0): session.terminate(ident)

def handleSession():
    msgkind = c.kind
    if(msgkind == "connect"): initSession()
    if(msgkind == "disconnect"): endSession()
