from session import *

class mail_counter(session):
    minute = 0

    def __init__(self, timestamp):
        self.counter = 0

        # the timestamp is in microseconds, so
        #timestamp % (10**6*60) is the number of microseconds
        # since the current minute started
        timestamp_minute = timestamp - (timestamp % (10**6*60))

        self.beginning = timestamp_minute // 10**6
        if mail_counter.minute < timestamp_minute:
            mail_counter.minute = timestamp_minute

    def end(self, reason):
        b.println(f"{self.beginning} {self.counter}")

    def update_time():
        """ register all mail_counter instances we need
            (there might be no messages in some minutes)
        """
        t = c.timestamp
        t -= t%(10**6*60)
        if mail_counter.minute == 0:
            mail_counter.minute = t - 10**6*60
        while mail_counter.minute < t:
            t2 = mail_counter.minute + 10**6*60
            mail_counter.get(
                str(t2),
                timestamp=t2,
                start_time=t2,
                timeout=60
            )

def inccounter():
    t = c.timestamp
    t -= t%(10**6*60)
    update_time()
    current = mail_counter.find(str(t))

    assert current is not None
    current.counter += 1

def update_time():
    mail_counter.update_time()
