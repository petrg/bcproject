from session import *

counter = 0
current_minute = None

def inccounter():
    global counter
    update_time()
    counter += 1

def print_counter():
    global counter
    global current_minute
    b.println(f"{current_minute} {counter}")
    counter = 0
    current_minute += 60

def update_time():
    global current_minute
    t = c.timestamp
    t = t//10**6
    t -= t%60
    if current_minute is None: current_minute = t
    while current_minute < t: print_counter()

class end(session):
    def end(self, reason):
        print_counter()

end.get("", timeout=0)
