// implementation of *option* node from the tree structure (having a regular expression, and if it matches the specified text,
// the control descends into the subtree bellow that node)

#ifndef MATCH_OPTION
#define MATCH_OPTION
#include <memory>
#include <vector>
#include "GrokIntegration.hpp"
#include "ParseAction.hpp"
#include "Context.hpp"

namespace parsing {
class ParseAction;

struct MatchOption {
    explicit MatchOption(std::string&& pattern, const std::vector<std::string>& patternFiles);
    MatchOption(MatchOption&& option) = default;
    MatchOption(MatchOption& option) = default;

    std::vector<ParseAction*> actions;
    std::vector<std::pair<int,bool>> attributeIndexes;
    GrokIntegration grok;
    bool breakFlag;
};
}
#endif
