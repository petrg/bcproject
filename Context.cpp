#include "Context.hpp"

namespace parsing {

//struct Result

Result::Result(str* attrs, std::unique_ptr<std::list<std::string>> owner, std::unique_ptr<std::map<str,str>> additional)
  : attributes(std::move(attrs)), attributeOwner(std::move(owner)), additionalAttributes(std::move(additional)) {}

Result::Result(Result&& other) noexcept {
    attributes = other.attributes;
    other.attributes = nullptr;
    attributeOwner = std::move(other.attributeOwner);
    additionalAttributes = std::move(other.additionalAttributes);
}

Result::~Result() {
    if (attributes != nullptr) delete[] attributes;
}

//class Context

Context::Context(str originalMsg) : _attributeOwner(),_originalMsg(originalMsg) {
    _finalAttributes = new str[_finalAttributeIndexes.size()];
    _intermediateAttributes = new str[_intermediateAttributeIndexes.size()];
    _finalAttributes[0] = originalMsg;
    _timestamp = 0;
    memset(_facility,0,sizeof(_facility));
    memset(_severity,0,sizeof(_severity));
}

Context::~Context() {
    delete[] _intermediateAttributes;
    if (_finalAttributes != nullptr) delete[] _finalAttributes;
}

void Context::setAttribute(str name, str attribute) {
    bool attrIsFinal = isFinal(name);
    auto* dest = attrIsFinal ? _finalAttributes : _intermediateAttributes;
    int index = (attrIsFinal ? _finalAttributeIndexes : _intermediateAttributeIndexes)[name];
    dest[index] = attribute;
}

str& Context::getAttribute(str name) {
    bool attrFinal = isFinal(name);
    auto* source = attrFinal?_finalAttributes:_intermediateAttributes;
    auto& indexes = attrFinal?_finalAttributeIndexes:_intermediateAttributeIndexes;
    auto indexIt = indexes.find(name);
    if (indexIt != indexes.end()) {
        return source[indexIt->second];
    } else if (_attributeOwner != nullptr && _additionalAttributes != nullptr) {
        auto attrIt = _additionalAttributes->find(name);
        if (attrIt != _additionalAttributes->end()) return attrIt->second;
    }
    throw std::runtime_error("could not find attribute \""+toString(name)+"\"");
}

str& Context::getAttribute(const std::string& name) {
    str weak_name(const_cast<std::string&>(name));//only needed to meet formal interface, no modifications are made
    return getAttribute(weak_name);
}

void Context::addOrSetAttr(const char* name, std::string value) {
    if (_attributeOwner == nullptr) _attributeOwner = std::make_unique<std::list<std::string>>();
    _attributeOwner->push_back(std::move(value));
    std::string& insertedValue = _attributeOwner->back();
    str weak_value(insertedValue);
    str weak_name(const_cast<char*>(name));//only needed to meet formal interface, no modifications are made
    bool attrFinal = isFinal(weak_name);
    auto& indexes = attrFinal ? _finalAttributeIndexes : _intermediateAttributeIndexes;
    auto indexIt = indexes.find(weak_name);
    if (indexIt != indexes.end()) {
        (attrFinal ? _finalAttributes : _intermediateAttributes)[indexIt->second] = weak_value;
    } else {
        _attributeOwner->push_back(toString(weak_name));
        weak_name = str(_attributeOwner->back());
        if (_additionalAttributes == nullptr) _additionalAttributes = std::make_unique<std::map<str,str>>();
        auto emplaceRes = _additionalAttributes->emplace(weak_name,weak_value);
        if (!emplaceRes.second) {
            emplaceRes.first->second = weak_value;
        }
    }
}

void Context::setAttribute(int index, bool isFinal, str value) {
    (isFinal ? _finalAttributes : _intermediateAttributes)[index] = value;
}

void Context::setTimestamp(timeType time) {
    _timestamp = time;
}

timeType Context::getTimestamp() const {
    return _timestamp;
}

#define TIMESTAMP_INDEX 1
str& Context::getAttribute(int index, bool isFinal) {
    if (index == TIMESTAMP_INDEX && isFinal) {
        _timeString = std::to_string(_timestamp);
        _timeStr = str(_timeString);
        return _timeStr;
    }
    return (isFinal ? _finalAttributes : _intermediateAttributes)[index];
}

Result Context::extractResults() {
    Result ret(_finalAttributes,std::move(_attributeOwner),std::move(_additionalAttributes));
    _finalAttributes = nullptr;
    return ret;
}

void Context::reinitialize(str originalMsg) {
    if (_finalAttributes == nullptr) {
        _finalAttributes = new str[_finalAttributeIndexes.size()];
    } else {
        for (int i=0; i<_finalAttributeIndexes.size(); i++) _finalAttributes[i] = str();
    }
    _attributeOwner = nullptr;
    _additionalAttributes = nullptr;
    _finalAttributes[0] = originalMsg;
    _originalMsg = originalMsg;
    for (int i=3; i<_intermediateAttributeIndexes.size(); i++) _intermediateAttributes[i] = str();
    _timestamp = 0;
}

const str* Context::getFinalAttributes() const {
    return _finalAttributes;
}

const std::map<str,str>* Context::getAdditionalAttributes() const {
    return _additionalAttributes.get();
}

str& Context::getOriginalMsg() {
    return _originalMsg;
}

void Context::setPriority(char* facility, char* severity) {
    strncpy(_facility,facility,sizeof(_facility));
    strncpy(_severity,severity,sizeof(severity));
    static bool attrsSet = false;
    if (!attrsSet) {
        setAttribute(1,false,str(_facility));
        setAttribute(2,false,str(_severity));
        attrsSet = true;
    }
}

const std::map<str,int>& Context::getFinalAttributeIndexes() {
    return _finalAttributeIndexes;
}

bool Context::isFinal(str attrName) {
    return attrName.length > 0 && attrName[0] != '_';
}

bool Context::isFinal(const std::string& attrName) {
    str weakName(const_cast<std::string&>(attrName));//only to meet the formal interface, no modifications made
    return isFinal(weakName);
}

int Context::addAttribute(std::string name) {
    if (_attributeNames.size() == 0) {
        if (_intermediateAttributeIndexes.size() != 0)
            throw std::runtime_error("empty Context::_attributeNames but not-empty _attributeNameIndexes");
        _attributeNames.push_back("msg");
        _finalAttributeIndexes.emplace(str(_attributeNames.back()),0);

        _attributeNames.push_back("timestamp");
        _finalAttributeIndexes.emplace(str(_attributeNames.back()),TIMESTAMP_INDEX);

        _attributeNames.push_back("facility");
        _finalAttributeIndexes.emplace(str(_attributeNames.back()),2);

        _attributeNames.push_back("severity");
        _finalAttributeIndexes.emplace(str(_attributeNames.back()),3);
    }
    str weakName(name);
    std::map<str,int>& indexes = isFinal(weakName) ? _finalAttributeIndexes : _intermediateAttributeIndexes;
    auto foundAttribute = indexes.find(weakName);
    if (foundAttribute != indexes.end()) return foundAttribute->second;
    int index = indexes.size();
    _attributeNames.push_back(std::move(name));
    indexes.emplace(str(_attributeNames.back()),index);
    return index;
}

std::map<str,int> Context::_finalAttributeIndexes = std::map<str,int>();
std::map<str,int> Context::_intermediateAttributeIndexes = std::map<str,int>();
std::list<std::string> Context::_attributeNames = std::list<std::string>();

}
