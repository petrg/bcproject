objectfiles = ParseAction.o MatchOption.o Context.o GrokIntegration.o ConfigParser.o utils.o TreeStorage.o python/CallAction.o python/python_integration.o main.o
all = $(objectfiles) beaver
path := $(shell pwd)
python_includes := $(shell python3-config --includes)
optimizer = -O3
debug: optimizer =
cflags = -std=c++17 $(optimizer)

all: $(all)

%.o: %.cpp
	g++ $(cflags) -c -o $@ $<

ParseAction.o: ParseAction.hpp ParseAction.cpp MatchOption.hpp Context.hpp
MatchOption.o: MatchOption.hpp MatchOption.cpp GrokIntegration.hpp ParseAction.hpp Context.hpp
Context.o: Context.hpp Context.cpp utils.hpp
GrokIntegration.o: GrokIntegration.hpp GrokIntegration.cpp utils.hpp Context.hpp
ConfigParser.o: ConfigParser.hpp ConfigParser.cpp ParseAction.hpp MatchOption.hpp python/CallAction.hpp utils.hpp
	g++ $(cflags) -o ConfigParser.o -c $(python_includes) ConfigParser.cpp

utils.o: cflags += -fPIC
utils.o: utils.hpp utils.cpp

TreeStorage.o: cflags += -fPIC
TreeStorage.o: TreeStorage.hpp TreeStorage.cpp

python/CallAction.o: python/CallAction.hpp python/CallAction.cpp ParseAction.hpp Context.hpp python/python_integration.hpp
	g++ $(cflags) -fPIE -o python/CallAction.o -c $(python_includes) -Wno-unused-variable -Wno-unused-function python/CallAction.cpp

python/python_integration.o: python/python_integration.hpp python/python_integration.cpp Context.hpp utils.hpp
	g++ $(cflags) -fPIE -o python/python_integration.o -c $(python_includes) python/python_integration.cpp
main.o: main.cpp ConfigParser.hpp Context.hpp
	g++ $(cflags) -o main.o -c $(python_includes) main.cpp

beaver: ParseAction.o MatchOption.o Context.o GrokIntegration.o ConfigParser.o utils.o python/CallAction.o python/python_integration.o main.o
	g++ $(cflags) -o beaver $(objectfiles) -lgrok $(shell python3-config --ldflags --embed >/dev/null && python3-config --ldflags --embed || python3-config --ldflags)

debug: cflags += -g3
debug: $(all)

clean:
	rm -r $(shell ls -d $(all) python/build python/__pycache__ 2>/dev/null)
install: $(all)
	cp beaver /usr/local/bin
	mkdir /usr/local/lib/beaver
	cp python/session.py /usr/local/lib/beaver
	cp python/empty.py /usr/local/lib/beaver

remove:
	rm /usr/local/bin/beaver
	rm -r /usr/local/lib/beaver
